/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejerciciosjunit;

/**
 *
 * @author nachorod
 */
public class Ejercicio1 {

    public static void main(String[] args) {
        Ejercicio1 emp1=new Ejercicio1();
        try {
            System.out.println(emp1.calculaSalarioBruto(null, 2000, 8));
        } catch (BRException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    

    
    // tipoEmpleado 0 - vendedor 1 - encargado
    public float calculaSalarioBruto(TipoEmpleado tipo, float ventasMes, float horasExtra) throws BRException {
        float salarioBruto=0;
        float salarioBase=0;
        int prima=0;
        int precioHoraExtra=20;

        if (tipo==TipoEmpleado.vendedor) {
            salarioBase=1000;
        } else if (tipo==TipoEmpleado.encargado) {
            salarioBase=1500;
        } else if (tipo==null) {
            throw new BRException("Tipo nulo no válido");
        }
        
        if (ventasMes>=1000 && ventasMes<1500) {
            prima=100;
        } else if (ventasMes>=1500) {
            prima=200;
        } else if (ventasMes<0) {
            throw new BRException("Ventas Mes negativas");
        }
        
        if (horasExtra<0) {
            throw new BRException("Horas extras negativas");
        }
        
        float importeHorasExtra=precioHoraExtra*horasExtra;
        salarioBruto=salarioBase+prima+importeHorasExtra;
        return salarioBruto;
    }

    public float calculaSalarioNeto(float salarioBruto) throws BRException {
        float salarioNeto=0;
        float retencion=0;
        if (salarioBruto>1000 && salarioBruto<=1500) {
            retencion=salarioBruto*(float)0.16;
        } else if (salarioBruto>1500) {
            retencion=salarioBruto*(float) 0.18;
        }
        salarioNeto=salarioBruto - retencion;
        if (salarioNeto<0) {
            throw new BRException("SalarioNeto negativo");
        }
        return salarioNeto;
    }
}

class BRException extends Exception {
    public BRException(String msj) {
        super(msj);
    }
}

enum TipoEmpleado {
    vendedor, encargado;
}

