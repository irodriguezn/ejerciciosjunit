/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosjunit;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author nachorod
 */
@RunWith(Parameterized.class)
public class Ejercicio1Test {
    
    float salarioBruto;
    float resultado;
    
    
    public Ejercicio1Test(float salarioBruto, float resultado) {
        this.salarioBruto=salarioBruto;
        this.resultado=resultado;
    }
    
    @Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][]{
            {2000,1640},
            {1500, 1230},
            {1499.99,1259.9916},
            {1250, 1050},
            {1000, 840},
            {999.99, 999.99},
            {500, 500},
            {0, 0},
            {-1, -1}
        });
    }    

    /**
     * Test of calculaSalarioBruto method, of class Ejercicio1.
     */
    //@Test
    /*public void testCalculaSalarioBruto() {
        System.out.println("calculaSalarioBruto");
        int tipo = 0;
        float ventasMes = 0.0F;
        float horasExtra = 0.0F;
        Ejercicio1 instance = null;
        float expResult = 0.0F;
        float result = instance.calculaSalarioBruto(tipo, ventasMes, horasExtra);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of calculaSalarioNeto method, of class Ejercicio1.
     */
    @Test
    public void testCalculaSalarioNeto() {
        System.out.println("calculaSalarioNeto");
        try {
            assertEquals(this.resultado, new Ejercicio1().calculaSalarioNeto(this.salarioBruto), 0.0);
        } catch (Exception e) {
            System.out.println("Sueldo negativo");
        }    
    }
}
